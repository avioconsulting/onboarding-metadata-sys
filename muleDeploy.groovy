muleDeploy {
    // version of the tool
    version '1.0'

    apiSpecification {
        name 'Onboarding Metadata Sys'
    }

    policies {
        clientEnforcementPolicyBasic {
            // version is optional (will use version in this library by default)
            version '1.2.3'
            // can supply paths just like above if necessary
        }
    }

    cloudHubApplication {
        environment params.env
        workerSpecs {
            muleVersion '4.3.0'
            usePersistentQueues true
            workerType WorkerTypes().micro
            workerCount 1
          //  awsRegion AwsRegions().uswest1
            customLog4j2Enabled false
            staticIpEnabled false
            objectStoreV2Enabled false
        }
       // analyticsAgentEnabled true
        file params.appArtifact
        cryptoKey params.cryptoKey
        autoDiscovery {
            clientId params.autoDiscClientId
            clientSecret params.autoDiscClientSecret
        }
        cloudHubAppPrefix 'onboarding'
        // optional from here on out
        /*
        appProperties([
                someProp: 'someValue'
        ])
        otherCloudHubProperties([
                some_ch_value_we_havent_covered_yet: true
        ])
        */
    }

}